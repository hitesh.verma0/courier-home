<?php
/**
 * 
 */
class chmDbquery{ 
	public $db=array();
	function __construct(){ 
		global $wpdb; 
		$this->db=$wpdb;
	}
	
	public static function _chm_checkWarehouse(){  
		$userId=get_current_user_id(); 
		$table_name = (new self())->db->prefix.CHMPRIFIX."warehouses"; 
		$sql = 'SELECT count(`id`) FROM `'.$table_name.'` WHERE `user_id` = %d'; 
		return (new self())->db->get_var((new self())->db->prepare($sql, array($userId))); 
	}
	
	public static function _chm_addWarehouse($values){  
		delete_transient('cache_chm_defaultwarehouse');
		$table_name = (new self())->db->prefix.CHMPRIFIX."warehouses";  
		$place_holder = "('%s','%s','%s', '%s', '%s','%d','%d','%s','%s')";
		$query = "INSERT INTO $table_name (`sender_name`,`email`,`mobile`,`address`,`pincode`,`user_id`,`status`,`created`,`modified`) VALUES $place_holder "; 
		return (new self())->db->query((new self())->db->prepare("$query ", $values));  	
	} 
	

	public static function chm_getDefaultWarehouse(){    
		delete_transient('cache_chm_defaultwarehouse'); 
		$defaultWarehouse = get_transient('cache_chm_defaultwarehouse');  
		if(false===$defaultWarehouse){ 
			$day=365;
			$expiry=86400*$day;
			$warehouses = (new self())->db->prefix.CHMPRIFIX."warehouses"; 
			$userTbl=(new self())->db->postmeta; 
			$userTbl = (new self())->db->prefix."users"; 
			$status=1;
			// $sql = 'SELECT * FROM `'.$warehouses.'` INNER JOIN `'.$userTbl.'`  ON `'.$warehouses.'`.`user_id` = `'.$userTbl.'`.`ID` WHERE `user_id` = "%d" AND `status`= "%d"'; 
			$sql = 'SELECT * FROM `'.$warehouses.'` INNER JOIN `'.$userTbl.'`  ON `'.$warehouses.'`.`user_id` = `'.$userTbl.'`.`ID` WHERE `status`= "%d"'; 
			$defaultWarehouse=(new self())->db->get_row((new self())->db->prepare($sql, array($status)),"ARRAY_A");  
			set_transient('cache_chm_defaultwarehouse', $defaultWarehouse,$expiry);
		 }
		return $defaultWarehouse; 
	}

	public static function _chm_setDefaultWarehouse($defaultId){  
		$userId=get_current_user_id(); 
		$table_name = (new self())->db->prefix.CHMPRIFIX."warehouses";  
		$place_holder = "('%d','%s')";
		// $query = "UPDATE $table_name (`status`,`modified`) VALUES $place_holder "; 

		(new self())->db->update( 
			$table_name, 
			array( 
				'status' =>0,  // string 
			), 
			array( 'user_id' =>$userId)
		);
		return (new self())->db->update( 
			$table_name,
			array( 
				'status' =>1,  // string
				'modified' => date("Y-m-d H:i:s")   // integer (number) 
			), 
			array( 'id' =>$defaultId)
		);
		 
 

	}

	public static function chm_getNotShipmentCreatedOrders($order_status = array(['wc-processing']),$limit="100"){ 
		$posts=(new self())->db->posts; 	
		$postmeta=(new self())->db->postmeta; 	 
		$sql="SELECT `ID` FROM {$posts}  WHERE `$posts`.`post_type` = 'shop_order' AND `$posts`.`post_status` IN ( '" . implode( "','", $order_status ) . "' ) 
		AND NOT EXISTS(SELECT * FROM `$postmeta` WHERE `$postmeta`.`meta_key` = 'chm_shipment_track_id' AND `$postmeta`.`post_id`=$posts.ID)  ORDER BY `ID` DESC LIMIT 0,$limit";
		$results = (new self())->db->get_results($sql,"ARRAY_A");    
		return $results;
	}

	public static function chm_filterOrders($customCondi ="",$limit="",$offset="0"){ 
		$offset=(!empty($offset))?"$offset , ":"";
		$limit=(!empty($limit))?" $limit ":"250";
		$posts=(new self())->db->posts; 	 
		$condition="WHERE `posts`.`post_type` = 'shop_order' ";
		if(!empty($customCondi)){
			$condition .=' AND'.$customCondi; 
		}
		$sql="SELECT `ID` FROM {$posts} as posts $condition ORDER BY `ID` DESC LIMIT $offset $limit"; 
		$results = (new self())->db->get_results($sql,"ARRAY_A");
		return $results;
	}

	public static function chm_filterOrderCounts($customCondi =""){ 
			$posts=(new self())->db->posts; 	 
			$condition="WHERE `posts`.`post_type` = 'shop_order' ";
			if(!empty($customCondi)){
				$condition .=' AND'.$customCondi; 
			}
			$sql="SELECT count(`ID`) FROM {$posts} as posts $condition"; 
			return (new self())->db->get_var($sql);
	} 

	public static function chm_getOrdersByProdId( $product_id, $order_status = array( 'wc-completed' ) ){ 
		$prefix=(new self())->db->prefix;
		$posts=(new self())->db->posts;
		$results = (new self())->db->get_col("
			SELECT order_items.order_id
			FROM {$prefix}woocommerce_order_items as order_items
			LEFT JOIN {$prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
			LEFT JOIN {$posts} AS posts ON order_items.order_id = posts.ID
			WHERE posts.post_type = 'shop_order'
			AND posts.post_status IN ( '" . implode( "','", $order_status ) . "' )
			AND order_items.order_item_type = 'line_item'
			AND order_item_meta.meta_key = '_product_id'
			AND order_item_meta.meta_value = '$product_id'
		");
	
		return $results;
	}
}