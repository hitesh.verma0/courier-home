<?php    
trait chmWoo{   

    public function chmGetWooOrderAddress($shipping=[],$billing=[]){
      $receiver=$sender=[];
      // Shipping Address  
      $shippingFirstName=(!empty($shipping["first_name"]))?ucfirst($shipping["first_name"]):"";
      $shippingFullName=(!empty($shipping["last_name"]))?$shippingFirstName." ".$shipping["last_name"]:$shippingFirstName; 
      $shippingAddress1=(!empty($shipping["address_1"]))?$shipping["address_1"]:"";
      $shippingFullAddress=(!empty($shipping["address_2"]))?$shippingAddress1.", ".$shipping["address_2"]:$shippingAddress1;
      $shippingFullAddress .=(!empty($shipping["city"]))?", ".$shipping["city"]:"";
      $shippingFullAddress .=(!empty($shipping["state"]))?", ".$shipping["state"]:"";
      $shippingFullAddress .=(!empty($shipping["country"]))?", ".$shipping["country"]:"";
      $shippingPostcode=(!empty($shipping["postcode"]))?$shipping["postcode"]:"";
      $shippingEmail=(!empty($shipping["email"]))?$shipping["email"]:"";
      $shippingPhone=(!empty($shipping["phone"]))?$shipping["phone"]:"";

      $billingFirstName=(empty($shippingFirstName)&&!empty($billing["first_name"]))?$billing["first_name"]:$shippingFirstName;
      $receiver["receiver_name"]=(empty(trim($shippingFullName))&&!empty($billing["last_name"]))?ucfirst($billingFirstName)." ".$billing["last_name"]:$shippingFullName; 
      
      $billingAddress1=(empty($shipping["postcode"])&&!empty($billing["address_1"]))?$billing["address_1"]:$shippingFullAddress;
      
      $receiver["receiver_address"]=(empty($shipping["postcode"])&&!empty($billing["address_2"]))?$billingAddress1.", ".$billing["address_2"]:$billingAddress1;
      
      $receiver["receiver_address"] .=(empty($shipping["city"])&&!empty($billing["city"]))?", ".$billing["city"]:"";
      $receiver["receiver_address"] .=(empty($shipping["state"])&&!empty($billing["state"]))?", ".$billing["state"]:"";
      $receiver["receiver_address"] .=(empty($shipping["country"])&&!empty($billing["country"]))?", ".$billing["country"]:"";
      $receiver["receiver_pincode"]=(empty($shipping["postcode"])&&!empty($billing["postcode"]))?$billing["postcode"]:$shippingPostcode;
      $receiver["receiver_email"]=(empty($shipping["email"])&&!empty($billing["email"]))?$billing["email"]:$shippingEmail;
      $receiver["receiver_mobile_no"]=(empty($shipping["phone"])&&!empty($billing["phone"]))?$billing["phone"]:$shippingPhone;

      return $receiver;
    }

    public function chmGetWooOrderProducts($products=[]){ 
      $items=[]; 
      if(!empty($products)){
          foreach ($products as $key => $lineItem) { 
            $_product = $lineItem->get_product();   
            $_product = $lineItem->get_product();   
            $weight=$_product->get_weight();
            $length=$_product->get_length();
            $width=$_product->get_width();
            $height=$_product->get_height();  
            $prodType=$_product->get_type();
            $attr="";
            $comma="";
            if ( 'variation' === $_product->get_type() ) {
                  $variation_id = $_product->get_variation_id();
                  $variation    = new WC_Product_Variation( $variation_id );
                  $attributes   = $variation->get_attributes(); 
                  foreach ( $attributes as $attr_index => $value ) {
                    $attr .=$comma.$attr_index.":".ucfirst($value);
                    $comma=","; 
                  }
            }
  
            // $img=wp_get_attachment_image_src( get_post_thumbnail_id( $lineItem["product_id"]));
            $img=wp_get_attachment_url( $_product->get_image_id() );  
            // $prodPrice =$product->get_price();
            // if( $product->is_on_sale() ) {
            //   $prodPrice =$product->get_sale_price();
            // }
            $items[$key]["product_name"]=$lineItem['name'];
            $items[$key]["product_amount"]=$_product->get_price();
            $items[$key]["product_quantity"]=$lineItem->get_quantity();
            $items[$key]["product_subtotal"]=$lineItem->get_subtotal();
            $items[$key]["product_total"]=$lineItem->get_total();
            $items[$key]["total_tax"]=$lineItem->get_total_tax();
            $items[$key]["total_tax"]=$lineItem->get_total_tax();
            $items[$key]["product_description"]=$_product->get_description();
            $items[$key]["weight"]=(!empty($weight))?$weight:"0.5";
            $items[$key]["length"]=(!empty($length))?$length:"10";
            $items[$key]["width"]=(!empty($width))?$width:"10";
            $items[$key]["height"]=(!empty($height))?$height:"5"; 
            $items[$key]["product_variant"]=($prodType=="variation")?$attr:$lineItem['name'];
            $items[$key]["product_SKU"]=$_product->get_sku();
            $items[$key]["product_image_URL"]=(!empty($img))?$img:site_url("/"); 
            $items[$key]["product_type"]=$prodType;
          }
        }
        
        return array_values($items);
    }

    /* Default Get processing and pending, hold orders */

    public function chmWooOrders($request=[]){ 
      $requestData=[];
      $apiKey=get_option('chm_authed_api_key'); 
      $defaultWarehouse=$this->chm_getDefaultWarehouse();  
      // $statusArr = wc_get_order_statuses();
      if(!empty($request)&&$request["action"]=="chm_sync_orders"){ 
      $dateFrom = (!empty($request['date_from']))?$request['date_from']:""; 
      $filterStatus = (!empty($request['filter_status']))?explode(",",$request['filter_status']):['wc-processing','wc-pending'];
      $counts=$conditions="";
      if(!empty($dateFrom)){  
        $dateTo = (!empty($request['date_to'])) ?$request['date_to']:date("Y-m-d");
        if($dateFrom == $dateTo) {
          $conditions = "`post_date` LIKE '%$dateFrom%'";  
        }else{
          $conditions = " `posts`.`post_date` BETWEEN '$dateFrom 00:00:00 ' AND '$dateTo 23:59:59'"; 
        }
      }  

      if(!empty($filterStatus)){
        $reqStatus=$stComma="";
        if(is_array($filterStatus)){
          foreach($filterStatus as $status){
            $reqStatus .=$stComma."'$status'";
            $stComma=",";
          } 
        }else{
          $reqStatus="'$filterStatus'";
        }
        $and=(!empty($conditions))?"AND":""; 
        $conditions .=" $and `posts`.`post_status` IN ($reqStatus)"; 
      }
      
      $posts=$this->chm_filterOrders($conditions,$request['limit'],$request['offset']); 
      $requestData["total_order"]=$this->chm_filterOrderCounts($conditions); 
      
      }else{
        $posts=$this->chm_getNotShipmentCreatedOrders(['wc-processing','wc-pending']);
      }
       
      if(!empty($posts)){ 
        foreach($posts as $loop => $post){
          $requestData[$loop]["sender_data"]["sender_name"]=(!empty($defaultWarehouse["sender_name"]))?$defaultWarehouse["sender_name"]:"display_name"; 
          $requestData[$loop]["sender_data"]["sender_address"]=$defaultWarehouse["address"]; 
          $requestData[$loop]["sender_data"]["sender_email"]=(!empty($defaultWarehouse["email"]))?$defaultWarehouse["email"]:"";  
          $requestData[$loop]["sender_data"]["sender_pincode"]=$defaultWarehouse["pincode"];
          $requestData[$loop]["sender_data"]["sender_mobile_no"]=$defaultWarehouse["mobile"]; 
          $requestData[$loop]["sender_data"]["warehouse_no"]="";
          $requestData[$loop]["shipments"]["shipment_type"]="parcel";
          $requestData[$loop]["shipments"]["shipment_mod"]="prepaid";
          $requestData[$loop]["shipments"]["HSN_code"]=$requestData[$loop]["shipments"]["eway_no"]=""; 
          $requestData[$loop]["shipments"]["order_pickup"]="yes";
          $requestData[$loop]["shipments"]["order_pickup"]="yes";  
          $requestData[$loop]["shipments"]["quality_check"]="no"; 
          $requestData[$loop]["shipments"]["product_quality_check"]=[];

          $orderId=$post["ID"];
          
          $order = wc_get_order($orderId);   
           $orderData = $order->get_data(); // The Order data
           $requestData[$loop]["receiver_data"]=$this->chmGetWooOrderAddress($orderData["shipping"],$orderData["billing"]);

           $requestData[$loop]["shipments"]["order_no"]="chm".substr(strtolower($apiKey),0,2)."-".$orderId;
           $requestData[$loop]["shipments"]["order_display_no"]=$orderId;
           $requestData[$loop]["shipments"]["COD_amount"]=$orderData["total"];
           $requestData[$loop]["shipments"]["currency"]=$orderData["currency"];
           $requestData[$loop]["shipments"]["product_datails"]=$this->chmGetWooOrderProducts($order->get_items()); 
        }
      } 
      return $requestData;  
    }
    
    public function chmWooShipmentResponse($results=[]){ 
      foreach($results as $result){
        if($result->response=="success"){
          $response=$result->success;
          $splitStr=explode("-",$response->order_no); 
          $postId=end($splitStr); 
          $trackId=sanitize_text_field($response->order_id); 
          if (!get_post_meta($postId, 'chm_shipment_track_id', true)) {
            add_post_meta($postId, 'chm_shipment_track_id',$trackId);
          }else{
            update_post_meta($postId, 'chm_shipment_track_id',$trackId);
          } 
          return get_post_meta($postId, 'chm_shipment_track_id', true);
        }else{
          file_put_contents(CHM_DIR_PATH.'logs/syncOrderFailedResponse.txt', print_r(json_encode($result),true));
        }
      }
      return true;
    }

    public function chmWooUpdateOrderStatus($orderId,$status,$trackingId=""){  
          $order = new WC_Order($orderId); 
          $trackId=sanitize_text_field($trackingId); 
          if (!get_post_meta($orderId, 'chm_shipment_track_id', true)) {
            add_post_meta($orderId, 'chm_shipment_track_id',$trackId);
          }else{
            update_post_meta($orderId, 'chm_shipment_track_id',$trackId);
          } 
        return (!empty($order))?$order->update_status($status, 'order_note'):false; 
    }
 
    // Ref :https://www.businessbloomer.com/woocommerce-easily-get-product-info-title-sku-desc-product-object/
    public function chmWooProducts($request=[]){  
      $requestData=[];
      $apiKey=get_option('chm_authed_api_key'); 
      
      $dateFrom = (!empty($request['date_from']))?$request['date_from']:""; 
      $filterStatus = (!empty($request['filter_status']))?explode(",",$request['filter_status']):'publish';
      $counts=$conditions="";
      if(!empty($dateFrom)){  
        $dateTo = (!empty($request['date_to'])) ?$request['date_to']:date("Y-m-d");
        if($dateFrom == $dateTo) {
          $conditions = "`post_date` LIKE '%$dateFrom%'";  
        }else{
          $conditions = " `posts`.`post_date` BETWEEN '$dateFrom 00:00:00 ' AND '$dateTo 23:59:59'"; 
        }
      }  

      if(!empty($filterStatus)){
        $reqStatus=$stComma="";
        if(is_array($filterStatus)){
          foreach($filterStatus as $status){
            $reqStatus .=$stComma."'$status'";
            $stComma=",";
          } 
        }else{
          $reqStatus="'$filterStatus'";
        }
        $and=(!empty($conditions))?"AND":""; 
        $conditions .=" $and `posts`.`post_status` IN ($reqStatus)"; 
      }
      
      $posts=$this->chm_filterProducts($conditions,$request['limit'],$request['offset']); 
      $requestData["total"]=$this->chm_filterProductCounts($conditions); 
      $requestData["products"]=[];
      if(!empty($posts)){ 
        foreach($posts as $key => $post){ 
          $product = wc_get_product($post['ID']);
          $requestData["products"][$key]=$this->_productFormate($product); 
        }  
      } 
      return $requestData;  
    }

   
    private function _productFormate($_product){
          $weight=$_product->get_weight();
          $length=$_product->get_length();
          $width=$_product->get_width();
          $height=$_product->get_height();  
          $prodType=$_product->get_type();
          $attr=[];
          $str=$comma="";
          if ( 'variable' === $_product->get_type() ) { 
                $attributes   = $_product->get_attributes();  // Get all attributes
                foreach ( $attributes as $attr_index => $value ) {
                  $value= $_product->get_attribute($attr_index); // Get option of attributes
                  $str =$attr_index.":".ucfirst($value); 
                  $attr[] =$str;
                }
          }  
           $img=wp_get_attachment_url( $_product->get_image_id() );  
          // $prodPrice =$product->get_price();
          // if( $product->is_on_sale() ) {
          //   $prodPrice =$product->get_sale_price();
          // }
          $items["product_type"]=$_product->get_type();
          $items["product_name"]=$_product->get_name();
          $items["product_name"]=$_product->get_name();
          $items["product_description"]=$_product->get_description();
          $items["weight"]=(!empty($weight))?$weight:"0.5";
          $items["length"]=(!empty($length))?$length:"10";
          $items["width"]=(!empty($width))?$width:"10";
          $items["height"]=(!empty($height))?$height:"5"; 
          $items["product_amount"]=$_product->get_price();
          $items["product_is_on_sale"]=$_product->is_on_sale();
          $items["product_sale_price"]=$_product->get_sale_price();
          $items["product_variant"]=($prodType=="variable")?$attr:$items["product_name"];
          $items["product_SKU"]=$_product->get_sku();
          $items["product_image_URL"]=(!empty($img))?$img:site_url("/"); 
          $items["product_type"]=$prodType;
          $items["product_instock"]=$_product->get_stock_quantity(); 
          $childProducts=$_product->get_children(); // get variations
          $items["variants"]=[]; 
          if(!empty($childProducts)){
            foreach($childProducts as $index => $childProdId){
              $childProduct = wc_get_product($childProdId);
              $items["variants"][$index]=$this->_productFormate($childProduct); 
            }
            
          }
          // $items["product_default_attributes"]=$_product->get_default_attributes();
          // $_product->get_attribute( 'attributeid' );
        return $items;
    } 
}  