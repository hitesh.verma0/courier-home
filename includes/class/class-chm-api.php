<?php    
include_once "class-woo.php"; // using for get wooCommerce products

class ChmApi extends ChmBase  {  
  use chmWoo;
  
  function __construct(){  
		parent::__construct(); 
	}

	public function _chmCallApi($method){  
		add_action( "wp_ajax_$method",array($this,"$method"));
		add_action("wp_ajax_nopriv_$method",array($this,"$method")); 
  }
   
  public function chm_check_auth(){
    $status="success"; 
    $request=$this->chmRequests();
    $requestData=[];
    try { 
       
      $isAuthed=$this->chmCheckAuthentication();
      if(!empty($isAuthed)){  // Used For add warehouse details
        $svData=array(); 
        if(empty($request['chmMobile']) || empty($request['chmAddress']) || empty($request['chmEmail']) || empty($request['chmPincode']) || empty($request['chmName']) ){
          throw new Exception("Please fill all required field."); 
        }  
        $userId = get_current_user_id(); 
         foreach($request as $value){
            if(!empty($value) &&$value!="chm_check_auth"){
              $svData[]=$value;
            }
         }
         $svData[]=$userId;
         $isSetWarehouseExist=$this->_chm_checkWarehouse();
         $svData[]=(!empty($isSetWarehouseExist))?0:1; // Status
         $svData[]=date("Y-m-d H:i:s"); // created
         $svData[]=date("Y-m-d H:i:s"); // modified 
         if($this->_chm_addWarehouse($svData)){
          $message="success"; 
         }  
      }else{  // Check Auth
       
        if(empty($request['chm_api_key']) || empty($request['chm_secret_key'])){
          throw new Exception("Please fill all details."); 
        } 
        if(!empty($_POST)){
          $requestData["appid"]=trim($request['chm_api_key']);
          $requestData["key"]=trim($request['chm_secret_key']);
          $requestData["site_url"]=trim(site_url('/'));
          $requestData["platform"]= $this->platform; 
          $result=$this->chmCallCurlAction($requestData,"authorization");  // calling public method in static method 
          $res=json_decode($result);   
          if($res->response=="success"){ 
            $message="Your Activation has been completed. Please create your warehouse";
            $this->chmVerifiedAuth($request);
          }else{
            throw new Exception("Invalid App ID Or Secret Key!");
          }
        }else{
          throw new Exception("Invalid Methhod request!.");
        }
      }
    } catch (Exception $exc) {
      $status="failed"; 
      $message = $exc->getMessage();
    } 
     $this->createJson($status,$message);  
  }

  public function chmVerifiedAuth($data){ 
    update_option('is_chm_authed','1'); 
    if(get_option('chm_authed_api_key')!== false ) {
      update_option('chm_authed_api_key',$data['chm_api_key']);
      update_option('chm_authed_secret_key',$data['chm_secret_key']);    
    }else {
      add_option('chm_authed_api_key',$data['chm_api_key'],null,'no');
      add_option('chm_authed_secret_key',$data['chm_secret_key'],null,'no');
    } 
  } 

  private function chmGetOrderByPlatform($request=[]){
    $method=$this->chmGeneratePlatformMethodName("Orders");
    $orders= $this->$method($request);
    return $orders;
  }
  public function chm_create_shipments(){    
    try{
      $orders=$this->chmGetOrderByPlatform();
      if(!empty($orders)){
          $result= $this->chmCallCurlAction($orders,"syncOrder");  
          $res=json_decode($result);  
          if($res->response!="success"){ 
            throw new Exception("This order is not synced.!"); 
          }else{
            $method=$this->chmGeneratePlatformMethodName("ShipmentResponse");
            if($this->$method($res->result)){ 
              wp_safe_redirect(wp_get_referer() ,302);
              exit;
            }
          }   
      }else{
        wp_safe_redirect(wp_get_referer() ,302);
        exit;
      } 
      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      }   
  }

  public function chm_trackshipment(){  
    try{
        $message=$status="success"; 
        $request=$this->chmRequests();
        $response=$requestData=array();  
        $dataArr=array(); 
        $requestData["order_id"]=$request["track"]; 
        $result= $this->chmCallCurlAction($requestData,"tracking"); 
        // pr($result);die;
        $res=json_decode($result); 
        if($res->response!="success"){ 
          throw new Exception("This order is not synced.!"); 
        }else{ 
          $filtredData=$this->chmJsonRequests($res->result->tracking); 
          $dataArr["tracking"]=$filtredData["data"];
          $dataArr["currentShipment"]=end($filtredData["data"]); 
        } 
      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      } 
      $this->createJson($status,$message,$dataArr);  
      exit;
  }

  public function chm_setdefault_warehouse(){
    $status="success"; 
    $request=$this->chmRequests();
     
    $requestData=[];
    try { 
      $isAuthed=$this->chmCheckAuthentication(); 
      if(empty($isAuthed)){
        $this->chm_set_message("Error! Please Authenticate your plugin. ");
        wp_safe_redirect(admin_url('admin.php?page=chm-started'),302);
        exit;
      } 
       
      if(!empty($request["id"])){
        delete_transient('cache_chm_defaultwarehouse');
        $id=base64_decode($request["id"]);
        $this->_chm_setDefaultWarehouse($id);
      }
      $this->chm_set_message("Default warehouses successfully set. ");
      wp_safe_redirect(admin_url('admin.php?page=chm-warehouses'),302);
      exit;

      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      }   
  }


  public function chm_cacnelshipment(){  
    try{
        $message=$status="success"; 
        $request=$this->chmRequests();
        $response=$requestData=array();  
        $dataArr=array(); 
        $requestData["order_id"]=$request["track"];  
        $result= $this->chmCallCurlAction($requestData,"cancelshipment"); 
        
        $res=json_decode($result); 
        // pr($result); die;
        if($res->response!="success"){ 
          throw new Exception("This Track id not found !"); 
        }else{
          $message=$res->result->massage;
        }  
      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      } 
      $this->createJson($status,$message);  
      exit;
  }

  public function chm_sync_orders(){    
    $orders=[];
    $totalItem="";
    $message=$status="success"; 
    try{ 
        if(!(new self)->chmCheckRequestMethod('post')) {
          throw new Exception("Invalid Method Request.");
        } 
        $request=$this->chmRequests(); 
        $api_id=trim($request["api_id"]);
        $api_key=trim($request["api_key"]);
        $check=$this->chmCheckApiToken($api_id,$api_key);
        if(!$check){
          throw new Exception("Api key or token not valid. Please contact with administrator.");
        }

        $orders=$this->chmGetOrderByPlatform($request); 
        $totalItem=$orders["total_order"];
        unset($orders["total_order"]); 
        if(empty($orders)){
          throw new Exception("Orders not found!"); 
        }  
      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      }  
      $this->createJson($status,$message,$orders,$totalItem);  
      exit; 
  }


  public function chm_update_order_status(){    
    $orders=[];
    $totalItem="";
    $message=$status="success"; 
    try{ 
        if(!(new self)->chmCheckRequestMethod('post')) {
          throw new Exception("Invalid Method Request.");
        } 
        $request=$this->chmRequests(); 
        $api_id=trim($request["api_id"]);
        $api_key=trim($request["api_key"]);
        $check=$this->chmCheckApiToken($api_id,$api_key);
        if(!$check){
          throw new Exception("Api key or token not valid. Please contact with ".CHMPLUGINNAME." administrator.");
        }
        if(empty($request["order_id"])){
          throw new Exception("Please send at least one order id.");
        }
        $orderIds=$request["order_id"]; 
        $trackingId=$request["tracking_id"]; 
        $reqStatus=$request["status"];  
        $statusArr = wc_get_order_statuses(); 
        if(!isset($statusArr[$reqStatus])){
          throw new Exception("Order status not match. Please contact with ".CHMPLUGINNAME." administrator");
        }

        $method=$this->chmGeneratePlatformMethodName("UpdateOrderStatus");
        
        $splitStr=explode("-",$orderIds);
        $orderId=end($splitStr); 
        if(!$this->$method($orderId,trim($reqStatus),$trackingId)){
          throw new Exception("Order not found!.");
        }
        $message="Order status succefully updated.";     

      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      }  
      $this->createJson($status,$message,$orders,$totalItem);  
      exit; 
  }

  /*
  API: http://localhost/wordpress/wp-admin/admin-ajax.php?action=chm_sync_products
    api_id:CH2008689
  api_key:123456
  date_from:
  date_to:2021-09-30
  filter_status:publish
  limit:20
  offset:16
  
  */
  public function chm_sync_products(){    
    $orders=[];
    $totalItem="";
    $message=$status="success"; 
    try{ 
        if(!(new self)->chmCheckRequestMethod('post')) {
          throw new Exception("Invalid Method Request.");
        } 
        $request=$this->chmRequests(); 
        $api_id=trim($request["api_id"]);
        $api_key=trim($request["api_key"]);
        $check=$this->chmCheckApiToken($api_id,$api_key);
        if(!$check){
          throw new Exception("Api key or token not valid. Please contact with administrator.");
        }
        
        $orders=$this->chmGetProductsByPlatform($request); 
        $totalItem=$orders["total_order"];
        unset($orders["total_order"]); 
        if(empty($orders)){
          throw new Exception("Orders not found!"); 
        }  
      } catch (Exception $exc) {
        $status="failed"; 
        $message = $exc->getMessage();
      }  
      $this->createJson($status,$message,$orders,$totalItem);  
      exit; 
  }

  private function chmGetProductsByPlatform($request=[]){
    $method=$this->chmGeneratePlatformMethodName("Products");
    $products= $this->$method($request); 
    return $products;
  }
}        