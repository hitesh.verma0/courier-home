<?php  
/**
 * 
 */
class ChmBase  { 
	use chmDbquery; 
	use webhooks; 
	public $platform="Woo";
	public $db="";
	function __construct() {
		global $wpdb; 
		$this->db=$wpdb;
		$this->platform=$this->chmGetPlatform();
	}  
	private $apiEnds=["authorization"=>"authorization","tracking"=>"tracking","webhooksyncorder"=>"syncorder","syncOrder"=>"create_shipment","cancelshipment"=>"shipment_cancellation"];
	public function chmInitWpFilter(){
		add_filter( 'wc_order_statuses', array($this, 'chmWooCreateCustomOrderStatus')); 
		add_action( 'init',array($this, 'chmInitializeWcOrderCustomStatus'));
		add_filter('wc_order_is_editable', [$this, 'chmCheckWcStatusEditable'], 10, 2);
		add_action( 'admin_init',array($this,'chm_include_css'));
		add_action( 'in_admin_footer',array($this,'chm_include_js'));  
		$this->chmWooCallHooks();
	}
	public function chmGetCustomOrderList(){	
		$order_statuses=[];
		$order_statuses['wc-shipment-created'] = _x( 'Shipment Created', 'Order status', 'woocommerce' );
		$order_statuses['wc-shipment-in-transit'] = _x( 'Shipment In Transit', 'Order status', 'woocommerce' );
		$order_statuses['wc-shipment-delivered'] = _x( 'Shipment Delivered', 'Order status', 'woocommerce' );
		$order_statuses['wc-shipment-cancelled'] = _x( 'Shipment Cancelled', 'Order status', 'woocommerce' );
		$order_statuses['wc-shipment-return'] = _x( 'Shipment Return', 'Order status', 'woocommerce' );
		$order_statuses['wc-shipment-cancelled'] = _x( 'Shipment Cancelled', 'Order status', 'woocommerce' ); 
		$order_statuses['wc-out-for-delivery'] = _x( 'Out for Delivery', 'Order status', 'woocommerce');
		$order_statuses['wc-ndr'] = _x('NDR', 'Order status', 'woocommerce' ); 
		return $order_statuses;
	}
	// Register in wc_order_statuses.
	public function chmWooCreateCustomOrderStatus($order_statuses){ 
		$newCustomStatus=$this->chmGetCustomOrderList();
		$isAuthed=$this->chmCheckAuthentication();
		if($isAuthed){
			$order_statuses=array_merge($order_statuses,$newCustomStatus);
		}
		return $order_statuses;
	}

	public function chmInitializeWcOrderCustomStatus()
	{
		$newCustomStatus=$this->chmGetCustomOrderList();
		foreach($newCustomStatus as $slug => $status){ 
			$args = array(
				'label' => _x( $status, 'Order status', 'textdomain' ),
				'public' => true,
				'exclude_from_search' => false,
				'show_in_admin_all_list' => true,
				'show_in_admin_status_list' => true,
				'label_count' => _n_noop($status . '<span class="count">(%s)</span>', $status. '<span class="count">(%s)</span>')
			  );
		
			  register_post_status($slug, $args); 
		}
	}

	public function chmCheckWcStatusEditable ($editable, $order) {
		$newCustomStatus=$this->chmGetCustomOrderList();
		if (!empty($newCustomStatus)) {
		  foreach ($newCustomStatus as $slug => $status) {
			if ($status->editable && 'wc-' . $order->get_status() == $slug) {
			  $editable = true;
			}
		  }
		}
		return $editable;
	}

	/**
	 * Sets an error message to show to the current user after a redirect.
	 *
	 * @param string $message The error message text.
	 * @return bool Whether the message was saved.
	 */
	public function chm_set_message($message) {
		
		$key = sprintf('chm_message-%d',get_current_user_id());
		return set_transient($key,$message,2 );
	}

	/**
	 * Gets the error message to show to the current user after a redirect.
	 * @return string The error message text.
	 */

	public function chm_get_message() {
		$key = sprintf('chm_message-%d',get_current_user_id());
		return get_transient($key);
	}
 
 	
	/**
	 * Include Css
	 */
	public function chm_include_css() { 
	    $enablePlugin=get_option('is_chm_actived');
	    if($enablePlugin=='1'&&is_admin()){
			wp_enqueue_style('chm_custom-style', CHM_PLUGIN_URL.'css/chm_custom.css','1.0.0','all');  
			wp_enqueue_style('chm_loader-style', CHM_PLUGIN_URL.'css/chm_loader.css','1.0.0','all');  
	    }
	}
	/**	 Include Css 	 */
	public function chm_include_js() { 
	    $enablePlugin=get_option('is_chm_actived');  
	    if($enablePlugin=='1'&&is_admin()){
			 wp_enqueue_script('chm-admin-custom-js', CHM_PLUGIN_URL.'js/chm_custom.js',array('jquery'),'1.1');
		}	
	}
	 
 
	/*
	* @params: boolan $status, sting $message, array $data 
	* @Function use: createJson: convert request data into json
	* @created by: Hitesh verma 
	*/
	public function createJson($status,$message,$data="",$itemCount=""){
		if(!empty($data)){
			$data = array('status' => $status, 'message' =>$message,'total_item' =>$itemCount,'data' =>$data);
		}else{
			$data = array('status' => $status, 'message' =>$message);
		}
        echo json_encode($data);
        exit();
	}
	

	/*
	* @params: sting $filename, sting $body
	* @Function use: chmRequestLog: Used to maintain log
	* @created by: Hitesh verma 
	*/
	public function chmRequestLog($body,$filename="request_log.txt"){
		$error_log_enable=get_option('chm_enable_log'); 
		if($error_log_enable =="yes"){
		  $request=$this->chmRequests();
		  $action=(isset($request['action']))?$request['action']:'';
		  $error_message = " "; 
		  $error_message .= "Action : ".$action." - ".$body;
		  $error_message .= "\n"; 
		  $log_file = CHM_DIR_PATH . "logs/$filename";
		  ini_set("log_errors", FALSE);  
		  ini_set('error_log', $log_file); 
		  error_log($error_message); 
		}
		return true;
	} 
	

	/*
	* @params: sting $filename, sting $body
	* @Function use: chmRequestLog: Used to maintain log
	* @created by: Hitesh verma 
	*/
	public function chmErrorLog($body,$action="",$filename="request_log.txt"){
		$error_log_enable=get_option('chm_enable_log'); 
		if($error_log_enable =="yes"){
		  $request=$this->chmRequests();
		  $action=(isset($action))?$action:'';
		  $error_message = " "; 
		  $error_message .= "Action : ".$action." - ".$body;
		  $error_message .= "\n"; 
		  $log_file = CHM_DIR_PATH . "logs/$filename";
		  ini_set("log_errors", FALSE);  
		  ini_set('error_log', $log_file); 
		  error_log($error_message); 
		}
		return true;
	} 

	/*
	* @params: sting $api_token, sting $key
	* @Function use: chmCheckAuthentication: Common method check plugin Authenticated
	* @created by: Hitesh verma 
	*/
	public function chmCheckAuthentication($api_token='',$key=''){
		$isAuthed=get_option('is_chm_authed');
		$ky=get_option('chm_authed_api_key');
		$secret=get_option('chm_authed_secret_key');
		if(!empty($isAuthed)&&!empty($ky)&&!empty($secret)){ 
			return true;
		}
		return false;
	}

	/*
	* @params: sting $api_token, sting $key
	* @Function use: chmCheckApiToken: Common method check token on api request
	* @created by: Hitesh verma 
	*/
	public function chmCheckApiToken($api_token='',$key=''){
		$pass=get_option('chm_authed_api_key');
		$savkey=get_option('chm_authed_secret_key');
		if(!empty($key)&&!empty($api_token)&&$pass==$api_token&&$key==$savkey){
			return true;
		}
		return false;
	}  

	/*
	* @params: array $cbArray
	* @Function use: chmRequests: Filter the api requested data
	* @created by: Hitesh verma 
	*/
	public function chmRequests($cbArray=array()){
		$request=array(); 
		if(!empty($_REQUEST)){ 
			foreach($_REQUEST as $fieldKey =>$val) {
				if(!empty($cbArray)&&in_array($fieldKey,$cbArray)){
					$request[$fieldKey] =array_map( 'sanitize_text_field', $val);
				}else{ 
					if($fieldKey=="email"){
						$request[$fieldKey]=sanitize_email($val);
					}elseif($fieldKey=="UpdateDateTime"){
						$trackingDateTime=sanitize_text_field($val);
						$request[$fieldKey]=date("m, F Y H:i A",strtotime($trackingDateTime));
					}else{
						$request[$fieldKey]=sanitize_text_field($val);
					}
				}
			}			 
		}  
		return $request; 
	}

	/*
	* @params: json $decodeJsonData
	* @Function use: chmRequests: Filter the api requested data
	* @created by: Hitesh verma 
	*/
	public function chmJsonRequests($decodeJsonData,$cbArray=array()){ 
		$request=json_decode(json_encode($decodeJsonData), true); // convert Into array 
		if(is_array($decodeJsonData)){
			$request["data"]= $request ;
		} 
		if(!empty($request)){
			 
			// $this->chmRequestLog($body); 
			foreach($request as $fieldKey =>$val) {
				$request=(is_object($val))?(array)$request:$request;
				

				if(!empty($cbArray)&&in_array($fieldKey,$cbArray)){
					$request[$fieldKey] =array_map( 'sanitize_text_field', $val);
				}else{ 
					if($fieldKey=="data"){  // Use for multi array.
						foreach($request["data"] as $mulitKey =>$dataVal) {
							$request[$fieldKey][$mulitKey]=$this->_chmMultiArrSanitize($dataVal); 
						} 
					}else{
						if($fieldKey=="person_email" || $fieldKey=="email"){
							$request[$fieldKey]=sanitize_email($val);
						}elseif(in_array($fieldKey,array("tag_ids","usertag","posttag","pubid"))){
							$parsedArr=wp_parse_id_list($val);
							$request[$fieldKey] =($val!='all')?array_map( 'sanitize_text_field',$parsedArr):'all';  
						}elseif($fieldKey=="UpdateDateTime"){
							$trackingDateTime=sanitize_text_field($val);
							$request[$fieldKey]=date("m, F Y H:i A",strtotime($trackingDateTime));
						}else{
							$request[$fieldKey]=sanitize_text_field($val);
						}
					}
				}
			}			 
		}  
		return $request; 
	}

	public function _chmMultiArrSanitize($dataVal){ 
		$request=array();
		$dataVal=(array)$dataVal;
		foreach($dataVal as $mulitKey =>$val) {
			if($mulitKey=="person_email" || $mulitKey=="email"){
				$request[$mulitKey]=sanitize_email($val);
			}elseif($mulitKey=="UpdateDateTime"){
				$trackingDateTime=sanitize_text_field($val);
				$request[$mulitKey]=date("m, F Y H:i A",strtotime($trackingDateTime));
			}else{
				$request[$mulitKey]=sanitize_text_field($val);
			}
		}
		return $request;	
	} 
	/*
	* @params: string $apiIndex, array $requestData
	* @Function use: chmCallCurlAction: Call curl
	* @created by: Hitesh verma 
	*/
	public function chmCallCurlAction($requestData=[],$apiIndex){ 
		$url=APIBASEURL.$this->apiEnds[$apiIndex];
		$error_log_enable=get_option('chm_enable_log');   
		 
		if($error_log_enable =="yes"){
			if($apiIndex=="webhooksyncorder"){
				file_put_contents(CHM_DIR_PATH.'logs/syncOrderRequest.txt', print_r(json_encode($requestData),true));
			}else{
				file_put_contents(CHM_DIR_PATH.'logs/postrequest.txt', print_r(json_encode($requestData), true));  
			}
		}	
		
		$header = array(); 
		$header[] = 'Content-type: application/json';
		if($apiIndex!="authorization"){
			$header[] = 'Authorization: '.json_encode($this->chmGetAuthDetails());
		}  
		/*
		$args = array(
			'headers' =>$header,
			"method" =>"POST",
			"body"=>json_encode($requestData),
			"sslverify"=>false,
		);
		$res = wp_remote_get($url,$args);
		$result=wp_remote_retrieve_body($res); */
	 
		  $ch = curl_init();   
		  curl_setopt($ch, CURLOPT_HTTPHEADER,$header);  
		  curl_setopt($ch, CURLOPT_URL,$url);
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		  curl_setopt($ch, CURLOPT_POST, true); 
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
		  $result = curl_exec($ch); 
		  curl_close($ch); 
		  if($error_log_enable =="yes"){
			if($apiIndex=="syncOrder"){
				file_put_contents(CHM_DIR_PATH.'logs/syncOrderResponse.txt', print_r(json_encode($result),true));
			}else{
				file_put_contents(CHM_DIR_PATH.'logs/chmresponse.txt', print_r($result, true));
			} 
			
		  }
 		return $result;
	} 

	
	private function chmGetAuthDetails(){
		$headerRequest=array(); 
		$headerRequest["appid"]=get_option('chm_authed_api_key');
		$headerRequest["key"]=get_option('chm_authed_secret_key'); 
		return $headerRequest;
	}

	/*
	* @params: 
	* @Function use: chmGetPlatform: set E-commerce platform.
	* @created by: Hitesh verma 
	*/
	public function chmGetPlatform(){
		$platform=get_option('chm_platform');
		/* create traits according to platform like chmWoo
		Mthod name sould be chmWooMethodName chm+Platform+methodName
		*/
		return (!empty($platform))?$this->platform:"Woo";
	}

	/*
	* @params: 
	* @Function use: _genaratePlatformMethodName: Generate method name by string.
	* @created by: Hitesh verma 
	*/
	public function chmGeneratePlatformMethodName($method){
		return "chm".$this->platform."".$method;
	}
	
	
	/*
	* @params: 
	* @Function use: _checkfields: set required  validation
	* @created by: Hitesh verma 
	*/

	public function _checkfields($fields){
        foreach ($fields as $field => $parameater) {
            if(empty($parameater)){
                return str_replace("_"," ",$field)." is required.";
            }
        }
        return true;
    }

	/*
	* @params: 
	* @Function use: chmCheckRequestMethod: check request method
	* @created by: Hitesh verma 
	*/
	public function chmCheckRequestMethod($method){
        $this->request=$_SERVER['REQUEST_METHOD'];
        return (strtolower($_SERVER['REQUEST_METHOD'])===strtolower($method))?true:false;
    }
}
