<?php
/**
 * 
 */
include_once(ABSPATH . 'wp-includes/pluggable.php');
trait chmDbquery{  
	 

	 public function _chm_getUserId(){  
		$current_user = wp_get_current_user();
		return $current_user->ID; ;
	}  

	public function _chm_checkWarehouse(){  
		$table_name = $this->db->prefix.CHMPRIFIX."warehouses"; 
		$sql = 'SELECT count(`id`) FROM `'.$table_name.'` WHERE `user_id` = %d'; 
		return $this->db->get_var($this->db->prepare($sql, array($this->_chm_getUserId()))); 
	}
	
	public function _chm_addWarehouse($values){  
		delete_transient('cache_chm_defaultwarehouse');
		$table_name = $this->db->prefix.CHMPRIFIX."warehouses";  
		$place_holder = "('%s','%s','%s', '%s', '%s','%d','%d','%s','%s')";
		$query = "INSERT INTO $table_name (`sender_name`,`email`,`mobile`,`address`,`pincode`,`user_id`,`status`,`created`,`modified`) VALUES $place_holder "; 
		return $this->db->query($this->db->prepare("$query ", $values));  	
	} 
	

	public function chm_getDefaultWarehouse(){    
		delete_transient('cache_chm_defaultwarehouse'); 
		$defaultWarehouse = get_transient('cache_chm_defaultwarehouse');  
		if(false===$defaultWarehouse){ 
			$day=365;
			$expiry=86400*$day;
			$warehouses = $this->db->prefix.CHMPRIFIX."warehouses"; 
			$userTbl=$this->db->postmeta; 
			$userTbl = $this->db->prefix."users"; 
			$status=1;
			// $sql = 'SELECT * FROM `'.$warehouses.'` INNER JOIN `'.$userTbl.'`  ON `'.$warehouses.'`.`user_id` = `'.$userTbl.'`.`ID` WHERE `user_id` = "%d" AND `status`= "%d"'; 
			$sql = 'SELECT * FROM `'.$warehouses.'` INNER JOIN `'.$userTbl.'`  ON `'.$warehouses.'`.`user_id` = `'.$userTbl.'`.`ID` WHERE `status`= "%d"'; 
			$defaultWarehouse=$this->db->get_row($this->db->prepare($sql, array($status)),"ARRAY_A");  
			set_transient('cache_chm_defaultwarehouse', $defaultWarehouse,$expiry);
		 }
		return $defaultWarehouse; 
	}

	public function _chm_setDefaultWarehouse($defaultId){  
		$table_name = $this->db->prefix.CHMPRIFIX."warehouses";  
		$place_holder = "('%d','%s')";
		// $query = "UPDATE $table_name (`status`,`modified`) VALUES $place_holder "; 

		$this->db->update( 
			$table_name, 
			array( 
				'status' =>0,  // string 
			), 
			array( 'user_id' =>$this->_chm_getUserId())
		);
		return $this->db->update( 
			$table_name,
			array( 
				'status' =>1,  // string
				'modified' => date("Y-m-d H:i:s")   // integer (number) 
			), 
			array( 'id' =>$defaultId)
		);
		 
 

	}

	public function chm_getNotShipmentCreatedOrders($order_status = array(['wc-processing']),$limit="100"){ 
		$posts=$this->db->posts; 	
		$postmeta=$this->db->postmeta; 	 
		$sql="SELECT `ID` FROM {$posts}  WHERE `$posts`.`post_type` = 'shop_order' AND `$posts`.`post_status` IN ( '" . implode( "','", $order_status ) . "' ) 
		AND NOT EXISTS(SELECT * FROM `$postmeta` WHERE `$postmeta`.`meta_key` = 'chm_shipment_track_id' AND `$postmeta`.`post_id`=$posts.ID)  ORDER BY `ID` DESC LIMIT 0,$limit";
		$results = $this->db->get_results($sql,"ARRAY_A");    
		return $results;
	}

	public function chm_filterOrders($customCondi ="",$limit="",$offset="0"){ 
		$offset=(!empty($offset))?"$offset , ":"";
		$limit=(!empty($limit))?" $limit ":"250";
		$posts=$this->db->posts; 	 
		$condition="WHERE `posts`.`post_type` = 'shop_order' ";
		if(!empty($customCondi)){
			$condition .=' AND'.$customCondi; 
		}
		$sql="SELECT `ID` FROM {$posts} as posts $condition ORDER BY `ID` DESC LIMIT $offset $limit"; 
		$results = $this->db->get_results($sql,"ARRAY_A");
		return $results;
	}

	public function chm_filterOrderCounts($customCondi =""){ 
			$posts=$this->db->posts; 	 
			$condition="WHERE `posts`.`post_type` = 'shop_order' ";
			if(!empty($customCondi)){
				$condition .=' AND'.$customCondi; 
			}
			$sql="SELECT count(`ID`) FROM {$posts} as posts $condition"; 
			return $this->db->get_var($sql);
	}
	
	public function chm_filterProducts($customCondi ="",$limit="",$offset="0"){ 
		$offset=(!empty($offset))?"$offset , ":"";
		$limit=(!empty($limit))?" $limit ":"250";
		$posts=$this->db->posts; 	 
		$condition="WHERE `posts`.`post_type` = 'product' ";
		if(!empty($customCondi)){
			$condition .=' AND'.$customCondi; 
		}
		$sql="SELECT `ID` FROM {$posts} as posts $condition ORDER BY `ID` DESC LIMIT $offset $limit";
		$results = $this->db->get_results($sql,"ARRAY_A");
		return $results;
	}

	public function chm_filterProductCounts($customCondi =""){ 
		$posts=$this->db->posts; 	 
		$condition="WHERE `posts`.`post_type` = 'product' ";
		if(!empty($customCondi)){
			$condition .=' AND'.$customCondi; 
		}
		$sql="SELECT count(`ID`) FROM {$posts} as posts $condition"; 
		return $this->db->get_var($sql);
	}

	public function chm_getOrdersByProdId( $product_id, $order_status = array( 'wc-completed' ) ){ 
		$prefix=$this->db->prefix;
		$posts=$this->db->posts;
		$results = $this->db->get_col("
			SELECT order_items.order_id
			FROM {$prefix}woocommerce_order_items as order_items
			LEFT JOIN {$prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
			LEFT JOIN {$posts} AS posts ON order_items.order_id = posts.ID
			WHERE posts.post_type = 'shop_order'
			AND posts.post_status IN ( '" . implode( "','", $order_status ) . "' )
			AND order_items.order_item_type = 'line_item'
			AND order_item_meta.meta_key = '_product_id'
			AND order_item_meta.meta_value = '$product_id'
		");
	
		return $results;
	}
}