<?php  
include_once CHM_DIR_PATH."includes/class//class-woo.php"; // using for get wooCommerce products
trait Webhooks {   
	use chmWoo;
	public function chmWooCallHooks(){   
		add_action('woocommerce_thankyou',array($this,'wooSyncOrder'), 10,1);
	}  

	public function wooSyncOrder($orderId)
	{	
		try{
			$apiKey=get_option('chm_authed_api_key'); 
			$authSecKey=get_option('chm_authed_secret_key'); 
			$defaultWarehouse=$this->chm_getDefaultWarehouse();  
			
			$requestData["appid"]=trim($apiKey);
			$requestData["key"]=trim($authSecKey);
			$requestData["site_url"]=trim(site_url('/'));
			$requestData["platform"]= $this->platform; 

			$requestData["sender_data"]["sender_name"]=(!empty($defaultWarehouse["sender_name"]))?$defaultWarehouse["sender_name"]:"display_name"; 
			$requestData["sender_data"]["sender_address"]=$defaultWarehouse["address"]; 
			$requestData["sender_data"]["sender_email"]=(!empty($defaultWarehouse["email"]))?$defaultWarehouse["email"]:"";  
			$requestData["sender_data"]["sender_pincode"]=$defaultWarehouse["pincode"];
			$requestData["sender_data"]["sender_mobile_no"]=$defaultWarehouse["mobile"]; 
			$requestData["sender_data"]["warehouse_no"]="";
			$requestData["shipments"]["shipment_type"]="parcel";
			$requestData["shipments"]["HSN_code"]=$requestData["shipments"]["eway_no"]=""; 
			$requestData["shipments"]["order_pickup"]="yes";
			$requestData["shipments"]["order_pickup"]="yes";  
			$requestData["shipments"]["quality_check"]="no"; 
			$requestData["shipments"]["product_quality_check"]=[]; 
			$order = wc_get_order($orderId);   
			$orderData = $order->get_data(); // The Order data
			$requestData["shipments"]["financial_status"]=$orderData["status"];
			$requestData["shipments"]["status"]=$orderData["status"];
			$requestData["shipments"]["shipment_mod"]=$orderData["payment_method"];
			$requestData["shipments"]["payment_method_title"]=$orderData["payment_method_title"];
			$requestData["receiver_data"]=$this->chmGetWooOrderAddress($orderData["shipping"],$orderData["billing"]);

			$requestData["shipments"]["order_no"]="chm".substr(strtolower($apiKey),0,2)."-".$orderId;
			$requestData["shipments"]["order_display_no"]=$orderId;
			$requestData["shipments"]["COD_amount"]=$orderData["total"];
			$requestData["shipments"]["currency"]=$orderData["currency"];

			$requestData["shipments"]["product_datails"]=$this->chmGetWooOrderProducts($order->get_items()); 
			$result=$this->chmCallCurlAction($requestData,"webhooksyncorder");  // calling public method in static method 
			$res=json_decode($result);   
			if($res->response!="success"){ 
				$this->chmErrorLog("Order not created!","wooSyncOrder","webhooksyncorder.text");
			}
		} catch (Exception $exc) {
			$status="failed"; 
			$message = $exc->getMessage();
			$this->chmErrorLog($message,"wooSyncOrder","webhooksyncorder.text");
	  } 
	}

	
}

