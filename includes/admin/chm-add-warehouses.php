 
<?php include_once 'chm-header.php'; ?> 
	<h1><span class="dashicons dashicons-location"></span> Order Pickup Address</h1> 
	<div class="tab-container text-left mt-2"> 
		 
		<div class="tab-container text-left"> 
			<p>Please fill your pickup location. The <?php echo CHMPLUGINNAME; ?> will pick you order from this location. you can also add mulitple location but the order will pick from default address. </p> 
		</div>
		<form id="chmwarehouseform" method="post" action=""  autocomplete="off">
			<table class="form-table" role="presentation">
				<tbody>
					<tr> 
						<td>
						<label>Name  <span class="chm-text-red">*</span></label>	
						<input type="text" name="chm_sendername" id="chm-name" value="" placeholder="Enter Name" size="200" autocomplete="off" required />
						<!-- <br><small>Note: Your Courier Home API Key.</small> -->
						</td>
						<td>
							<label>Mobile Number <span class="chm-text-red">*</span></label>	
							<input type="text" name="chm_mobile" id="chm-mobile" value="" placeholder="Enter Mobile" size="150" autocomplete="off" minlength="10" maxlength="10" onkeypress="return isChmNumberField(event)" required />
							<!-- <br><small>Note: Your Courier Home API Key.</small> -->
						</td> 
					</tr>
					<tr> 
						<td>
						<label>Address  <span class="chm-text-red">*</span></label>	
						<input type="text" name="chm_address" id="chm-address" value="" placeholder="Enter Address" size="200" autocomplete="off" required />
						<!-- <br><small>Note: Your Courier Home API Key.</small> -->
						</td>
						<td>
							<label>Email  <span class="chm-text-red">*</span></label>	
							<input type="email" name="chm_email" id="chm-email" value="" placeholder="Enter Email" size="150" autocomplete="off" required />
							<!-- <br><small>Note: Your Courier Home API Key.</small> -->
						</td> 
					</tr>
					 <tr> 
					 	<td> 
							<label>Pincode  <span class="chm-text-red">*</span></label>	
							<input type="text" name="chm_pincode" id="chm-pincode" value="" placeholder="Enter Pincode" minlength="6" maxlength="6" onkeypress="return isChmNumberField(event)" size="150" autocomplete="off" required /> 
						</td> 
					</tr>
				</tbody>
			</table>
			<div style="margin: auto; left: 0; right: 0;  text-align: right;">
			<input type="hidden" name="chm_redirection" id="chm-redirection" value="<?php echo admin_url('admin.php?page=chm-warehouses'); ?>"/> 
				<input type="submit" name="submit" id="chmsubmit" class="button button-primary" value="Submit">
			</div>
			<div style="display:none;" id="errormsgbx" ></div>
			<div style="display: none;" id="loader" class="spinner-border text-danger" role="status">
				<span class="sr-only">Loading...</span>
			</div>
		</form 
	</div> 
</div> <!-- End chm-wrap -->
