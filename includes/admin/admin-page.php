<?php include_once 'chm-header.php'; ?>
<div class="ntz-wrap">
	<h1>CRM Memberships Settings</h1>
	<div class="wrap about__container">
		<div class="about__header1">
			<form method="post" action="options.php">
				<nav class="ntz-tabs wp-clearfix" aria-label="Secondary menu">
					<!-- <a href="#" class="nav-tab nav-tab-active" data-tab="tab-1"><span class="dashicons dashicons-admin-generic"></span> General Settings </a>  -->
				</nav>
				<div class="tab-container">
					<div id="tab-1" class="tab-content current">
						<?php settings_fields("section");
						do_settings_sections("commontheme-options"); ?>
					</div>
					<div id="tab-2" class="tab-content">
			  			<?php settings_fields("section");
						do_settings_sections("salesforce-settings"); ?>
					</div>
					<div id="tab-3" class="tab-content">
						<?php settings_fields("section");
						do_settings_sections("usertracking-options"); ?>
					</div>
					<div id="tab-4" class="tab-content">
						<?php settings_fields("section");
						do_settings_sections("emailtemplate-options"); ?>
					</div>
					<div class="m-0 p-0">
						<?php submit_button(); ?>
					</div>
				</div>
				<?php // wp_nonce_field( 'chm-admin-post' ); ?>
			</form>
		</div>
	</div>
</div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('a.nav-tab').click(function() {
					var tab_id = $(this).attr('data-tab');
					$('a.nav-tab').removeClass('nav-tab-active');
					$(this).addClass('nav-tab-active');
					$('.tab-content').removeClass('current');
					//		$(this).addClass('current');
					$("#" + tab_id).addClass('current');
				})

			});
		</script>