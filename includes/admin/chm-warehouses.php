<?php   
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class chm_warehouse_list extends WP_List_Table {
   /**
     * [REQUIRED] You must declare constructor and give some basic params
     */
    public $request;
    function __construct() {
    global $status, $page;
        parent::__construct(array(
            'singular' => 'Warehouse',
            'plural' => 'warehouses',
        ));
        $this->request=$this->chmRequests();
    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

   /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="id[]" value="%s" />', $item['ID']
        );
    }

    function get_columns() {
        $columns = array(
            // 'cb' => '<input type="checkbox" />',
            'sn'=> 'S/N',
            'sender_name' => __('Name', CHMPRIFIX.'warehouse_list'),
            'email' => __('Email', CHMPRIFIX.'warehouse_list'),
            'mobile' => __('Mobile', CHMPRIFIX.'warehouse_list'),
            'address' => __('Address', CHMPRIFIX.'warehouse_list'), 
            'pincode'=> __('Pincode', CHMPRIFIX.'warehouse_list'),
            'action' => __('Action', CHMPRIFIX.'warehouse_list'),
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array( 
            'pincode' => array('Pincode', false), 
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array();
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->users; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($this->request['id']) ? $this->request['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
              //  $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

    function prepare_items() { 
        global $wpdb;
        $userId = get_current_user_id(); 
        $conditions="WHERE user_id = $userId";
        $hidden=array();
        $per_page = 10;
        $columns = $this->get_columns();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
        
        $paged = isset($this->request['paged']) ? ($per_page * max(0, intval($this->request['paged']) - 1)) : 0;

        $orderby = (isset($this->request['orderby']) && in_array($this->request['orderby'], array_keys($this->get_sortable_columns())))? $this->request['orderby'] : 'status';
        
        $order = (isset($this->request['order']) && in_array($this->request['order'], array('asc', 'desc'))) ? ' '.strtoupper($this->request['order']) : ' DESC';
         
        if(isset($this->request['s'])&&!empty($this->request['s'])){
          $and=' AND ';
          $conditions .=" $and (`pincode` LIKE '%".sanitize_text_field($this->request['s'])."%')";
        }
        
        $table = $wpdb->prefix.CHMPRIFIX."warehouses";   
        $sql ='SELECT * FROM `'.$table.'` '.$conditions.'  LIMIT '.$paged.','.$per_page ;
        $total_items=$wpdb->get_var("SELECT count(id) FROM `$table` $conditions");
        $this->items=$wpdb->get_results($sql,ARRAY_A); 

        // pr($this->items);die;
        if(!empty($this->items)){
          foreach($this->items as $index => $value){
            $this->items[$index]["sn"]= ($index+1); 
            $this->items[$index]["action"]=(!empty($value["status"])&&$value["status"]==1)? "<label class='page-title-action button button-success'>Default</label>":"<a  href='".admin_url('/admin-ajax.php?action=chm_setdefault_warehouse&id='.base64_encode($value["id"]))."' class='page-title-action button'>Set Default</a>";
          }
        } 
        
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    } 
}

global $wpdb;
$request=$this->chmRequests();  
$table = new chm_warehouse_list(); 
$table->prepare_items(); 

include_once 'chm-header.php'; ?>
<style> 
th#sn {
    width: 5%;
}
</style>
    <div class="chm-wrap">
        <h1><?php _e('Order Pickup Address') ?></h1>  
        <!-- <script type="text/javascript">
            jQuery(document).ready( function($){
              jQuery(jQuery(".wrap .bulkactions")[0]).append('<a style="margin-top: 4px;" href="<?php echo admin_url('/admin-ajax.php?action=chm_export_subscriber&datefrom='.$DateFrom.'&dateto='.$DateTo.'&tag='.$filterTag.'');?>" class="page-title-action button">Export</a>');
            });
        </script> --> 
         <form id="online_admissions-table-date-range" method="GET">
         <input type="hidden" name="page" value="<?php  esc_html_e($request['page'],CHMPRIFIX); ?>"/>  
            <!--  <div style="display:inline-block;width: 100%;">
              <input type="submit" id="search-submit" class="button" value="<?php esc_attr_e("Filter",CHMPRIFIX);?>">   
            </div> -->
            <?php echo "<a  href='".admin_url('/admin.php?page=chm-add-warehouses')."' class='page-title-action button'>Add New</a>"; ?>
            <?php
             $table->search_box('Search By pincode', 'search');
             $table->display() ?>
             <?php wp_nonce_field( 'chm-sub-post' ); ?>
        </form>  
    </div>