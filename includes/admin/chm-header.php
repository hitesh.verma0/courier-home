<div class="chm-global-nav-menu">
	<div class="chm-title">
		<a href="admin.php?page=chm-started">
			<span class="screen-reader-text"><?php echo _e(CHMPLUGINNAME); ?></span><img class="chm-logo" src="<?php echo CHM_PLUGIN_URL."images/logo-white.png"; ?>" alt="<?php echo _e(CHMPLUGINNAME); ?>">
		</a>
	</div>

	<div class="chm-global-nav__items">
		<a class="chm-global-nav-menu__tab chm-global-nav-menu__one <?php echo (!empty($_GET["page"])&&$_GET["page"]=="chm-started")?"chm-menu-active":""; ?>" href="<?php echo admin_url('admin.php?page=chm-started')?>">Getting Started</a>
		<?php     
		if($this->isAuthed&&!empty($this->isSetWarehouseExist)){ ?>
			<a class="chm-global-nav-menu__tab chm-global-nav-menu__six <?php echo (!empty($_GET["page"])&&$_GET["page"]=="chm-warehouses")?"chm-menu-active":""; ?> " href="<?php echo admin_url('admin.php?page=chm-warehouses');?>">Order Pick Address</a>
			<!-- <a class="chm-global-nav-menu__tab chm-global-nav-menu__two <?php echo (!empty($_GET["page"])&&$_GET["page"]=="chm-settings")?"chm-menu-active":""; ?> " href="<?php echo admin_url('admin-ajax.php?action=chm_create_shipments');?>">Create Bulk order Shipment</a> -->
		<?php } ?> 
		<!-- <a class="chm-global-nav-menu__tab chm-global-nav-menu__two <?php echo (!empty($_GET["page"])&&$_GET["page"]=="chm-settings")?"chm-menu-active":""; ?> " href="admin.php?page=chm-activation">Settings</a>  --> 
	</div>

	<div class="chm-top-links">
		<a target="_blank" class="chm-top-links__item" title="Knowledge Base" href="<?php echo _e(CHMPLUGINURI); ?>"><span class="dashicons dashicons-book"></span></a>
		<a target="_blank" class="chm-top-links__item" title="Community" href="#"><span class="dashicons dashicons-youtube"></span></a>
		<a target="_blank" class="chm-top-links__item" title="Support" href="<?php echo _e(CHMPLUGINURI); ?>contact/"><span class="dashicons dashicons-phone"></span></a>
	</div>
</div>
 
 <div class="chm-wrap">


<?php if(!empty($_GET["page"])&&$_GET["page"]=="chm-started" && !$isAuthed){?>
	<h1>Get ready to start configuration</h1>
<?php }elseif(!empty($_GET["page"])&&$_GET["page"]=="chm-started" &&!empty($isAuthed)&&empty($isSetWarehouseExist)){ ?>
	<h1><span  class="dashicons dashicons-location"></span> Order Pickup Address</h1>
<?php }else if(isset($ptitle)){ ?>
	<h1><?php echo $ptitle?></h1>
<?php } ?>

	 <?php 
	$sessionMessage=$this->chm_get_message();
	if(!empty($sessionMessage)){ 
		 if (strpos($sessionMessage, 'Error!') !== false) { ?>
			<div id="chm-msgbox" class="card-panel border-danger" style="margin:20px;">
				<button aria-hidden="true" data-dismiss="alert" id="chm-removemsg" class="close button  float-right" type="button">×</button>
				<i class="icon fa fa-check"></i>
			<?php echo __($sessionMessage); ?>
		</div> 
	<?php }else{ ?>
		<div id="chm-msgbox" class="card-panel border-danger" style="margin:20px;">
				<button aria-hidden="true" data-dismiss="alert" id="chm-removemsg" class="button  close float-right" type="button">×</button>
				<i class="icon fa fa-check"></i>
				<?php echo __($sessionMessage); ?>
			</div> 
		<?php } ?> 
	<?php } ?> 

