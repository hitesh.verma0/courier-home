<?php  
include_once(CHM_DIR_PATH . "includes/admin/form-fields.php"); 
ob_start();
class chmAdmin extends ChmBase{ 
  public $isAuthed=""; 
  public $isSetWarehouseExist="";
  function __construct(){ 
		parent::__construct();  
    $this->isAuthed=$this->chmCheckAuthentication(); 
    $this->isSetWarehouseExist=$this->_chm_checkWarehouse();
	}
  public function chmInitAdminWpFilter(){
		add_action( 'admin_menu', array($this, 'chmAdminMenu')); 
		add_action("admin_init", [$this,"chm_display_theme_panel_fields"]); 
    add_filter('manage_users_custom_column',[$this,"chm_modify_user_table_row"], 10, 3); 
		// add_action('admin_bar_menu', [$this,'chmCustomToolbarLink'], 999); // Removed the sync order
	}

  
 
  function chmAdminMenu($value = ''){
    if (is_admin() && get_option('is_chm_actived') == '1') {
      add_menu_page(__('Courier Home Settings', 'courier_home'), __('Courier Home', 'courier_home'), 'activate_plugins', 'chm-started',[$this,'chmGettingStarted'],CHM_PLUGIN_URL.'images/chm.png', null);
      add_submenu_page('chm-started', 'Getting Started', 'Getting Started', 'manage_options', 'chm-started',[$this,'chmGettingStarted']); 
      
      if($this->isAuthed&&$this->isSetWarehouseExist){
        add_submenu_page('chm-started', 'Order Pickup Address', 'Order Pick Address', 'manage_options', 'chm-warehouses', [$this,'chm_warehouses']); 
        add_submenu_page('chm-started', 'Add Pickup Address', 'Add Pickup Address', 'manage_options', 'chm-add-warehouses', [$this,'chm_add_warehouses']); 
      }
      add_submenu_page('chm-started', 'Settings', 'Settings', 'manage_options', 'chm-settings',[$this,'chm_settings']); 
    }
  }
  
public function chmGettingStarted(){   
   include_once(CHM_DIR_PATH . "includes/admin/getting-started.php");
} 

function chm_settings()
{ 
  include_once(CHM_DIR_PATH . "includes/admin/admin-page.php");
}


  function chm_add_warehouses(){   
    if(empty($this->isAuthed)){
      wp_safe_redirect(admin_url('admin.php?page=chm-started'),302);
      exit;
    } 
    include_once(CHM_DIR_PATH . "includes/admin/chm-add-warehouses.php");
  }
  
  function chm_warehouses(){  
    include_once(CHM_DIR_PATH . "includes/admin/chm-warehouses.php");
  }


      function chm_display_theme_panel_fields(){ 
        add_settings_field("chm_enable_log", "Enable Logs", "chm_enable_log", "commontheme-options", "section");
        add_settings_section("section", "", null, "commontheme-options");   
        register_setting("section", "chm_enable_log");  
      }
      
      
      function chm_modify_user_table_row($row_output, $column_id_attr, $user){ 
        $date_format = 'M j, Y h:i A'; 
        switch ($column_id_attr) {
          case 'registration_date':
            return date($date_format, strtotime(get_the_author_meta('registered', $user)));
            break;
          default:
        }

        return $row_output;
      } 

  // add a link to the WP Toolbar
  function chmCustomToolbarLink($wp_admin_bar) {
    // Temprary Remove Create pending Shipment
    if($this->chmCheckAuthentication()){
      $args = array(
          'id' => 'wpbeginner',
          'title' =>'<span class="ab-icon"></span>'.__( 'Create pending order Shipment', 'some-textdomain' ), 
          'href' => admin_url('admin-ajax.php?action=chm_create_shipments'), 
          'meta' => array(
              'class' => 'chm-sync-icon', 
              'title'    => __( 'Courierhome Create pending order shipment in bulk', 'some-textdomain' ),   
              )
      );
      $wp_admin_bar->add_node($args);
    }
  }

  function chm_orderPostmeta(){  
    include_once CHM_DIR_PATH . "includes/admin/orderpostmeta.php";
  }

  function chm_orderShipmentStatus(){  
    echo "<table class='wp-list-table widefat fixed striped table-view-list posts'>
    <thead>
        <tr>
        <th style='width: 5%;' class='manage-column'>S/N.</th>
          <th class='manage-column'><span class='dashicons dashicons-info'></span> Status</th>
          <th class='manage-column'><span class='dashicons dashicons-location'></span> Location</th>
          <th class='manage-column'><span class='dashicons dashicons-calendar'></span> Last Update</th>
          
        </tr>
      </thead>
      <tbody id='chm-shipmentul' class='ch_order_notes'></tbody></table>";
  } 

}




