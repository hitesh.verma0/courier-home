 
<?php include_once 'chm-header.php'; ?> 
	 <div class="tab-container text-left mt-2"> 
		<?php  if(!$this->isAuthed){?>
			<div class="tab-container text-left"> 
				<p>Courierhome integrates with Your E-commerce store and offers hassle free shipping experience.</p>
				<p>To integrate with Courierhome’s You need to have Your API Id and secret key which You can generate from your Courierhome dashboard.</p>
				<p>Watch here: </p>
			</div>
			<form id="chm-auth-form" method="post" action=""  autocomplete="off">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row">Courier Home API ID <span class="chm-text-red">*</span></th>
							<td><input type="text" name="api_key" id="chm_api_key" value="" placeholder="Enter API Key" size="60" 60" autocomplete="off" required /><br><small>Note: Your Courier Home API ID.</small></td>
						</tr>
						<tr>
							<th scope="row">Courier Home Secret key  <span class="chm-text-red">*</span></th>
							<td><input type="password" name="secret_key" id="chm_secret_key" value=""  placeholder="Enter Secret key" size="60" autocomplete="off" required /><br>
							<small>Note: This unique key string get from Courier Home.</small>
							</td>
						</tr>
					</tbody>
				</table>
				<div style="margin: auto; left: 0; right: 0;  text-align: center;">
					<input type="submit" name="submit" id="chm-submit" class="button button-primary" value="Activate">
				</div>
				<div style="display:none;" id="errormsgbx" ></div>
				<div style="display: none;" id="loader" class="spinner-border text-danger" role="status">
					<span class="sr-only">Loading...</span>
				</div>
			</form>
			<div class="mt-2"> 
			For any support contact us at 80057-47571 or email us at <a href="mailto:merchant@courierhome.com">merchant@courierhome.com</a>
			</div>
		<?php }elseif(!empty($this->isAuthed) && empty($this->isSetWarehouseExist)){?>
			<div class="tab-container text-left"> 
				<p>Please fill your pickup location. The <?php echo CHMPLUGINNAME; ?> will pick you order from this location. you can also add mulitple location but the order will pick from default address. </p> 
			</div>
			<form id="chmwarehouseform" method="post" action=""  autocomplete="off">
				<table class="form-table" role="presentation">
					<tbody>
					<tr> 
						<td>
						<label>Name  <span class="chm-text-red">*</span></label>	
						<input type="text" name="chm_sendername" id="chm-name" value="" placeholder="Enter Name" size="200" autocomplete="off" required />
						<!-- <br><small>Note: Your Courier Home API ID.</small> -->
						</td>
						<td>
							<label>Mobile Number <span class="chm-text-red">*</span></label>	
							<input type="text" name="chm_mobile" id="chm-mobile" value="" placeholder="Enter Mobile" size="150" autocomplete="off" minlength="10" maxlength="10" onkeypress="return isChmNumberField(event)" required />
							<!-- <br><small>Note: Your Courier Home API ID.</small> -->
						</td> 
					</tr>
						<tr> 
							<td>
							<label>Address  <span class="chm-text-red">*</span></label>	
							<input type="text" name="chm_address" id="chm-address" value="" placeholder="Enter Address" size="200" autocomplete="off" required />
							<!-- <br><small>Note: Your Courier Home API ID.</small> -->
							</td>
							<td>
								<label>Email  <span class="chm-text-red">*</span></label>	
								<input type="email" name="chm_email" id="chm-email" value="" placeholder="Enter Email" size="150" autocomplete="off" required />
								<!-- <br><small>Note: Your Courier Home API ID.</small> -->
							</td> 
						</tr>
 						<tr>  
							<td> 
								<label>Pincode  <span class="chm-text-red">*</span></label>	
								<input type="text" name="chm_pincode" id="chm-pincode" value="" placeholder="Enter Pincode" minlength="6" maxlength="6" onkeypress="return isChmNumberField(event)" size="150" autocomplete="off" required /> 
							</td> 
						</tr>
					</tbody>
				</table>
				<div style="margin: auto; left: 0; right: 0;  text-align: right;">
				<input type="hidden" name="chm_redirection" id="chm-redirection" value=""/> 
					<input type="submit" name="submit" id="chmsubmit" class="button button-primary" value="Submit">
				</div>
				<div style="display:none;" id="errormsgbx" ></div>
				<div style="display: none;" id="loader" class="spinner-border text-danger" role="status">
					<span class="sr-only">Loading...</span>
				</div>
			</form>
		<?php }else{?>
			<div class="chm-authed" style="background:#FFA73B;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<!-- LOGO -->
					<tr>
						<td  align="center">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
								<tr>
									<td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding: 0px 10px 0px 10px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
								<tr>
									<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
										<h1 style="font-size: 48px; font-weight: 400; margin: 2;">Welcome!</h1> <img src="<?php echo CHM_PLUGIN_URL."images/handshake.png"; ?>" width="125" height="120" style="display: block; border: 0px;" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td  align="center" style="padding: 0px 10px 0px 10px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
								<tr>
									<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
										<p style="margin: 0;">We're excited to onboard. Thanks for choosing Courierhome, welcome to the world of efficiency.</p>
									</td>
								</tr>
								<!-- <tr>
									<td bgcolor="#ffffff" align="left">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td bgcolor="#ffffff" align="center" style="padding: 20px 30px 60px 30px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="center" style="border-radius: 3px;" bgcolor="#FFA73B"><a href="#" target="_blank" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;">Confirm Account</a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr> COPY -->
								<tr>
									<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 0px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
										<h5 style="margin: 0;">Courierhome offering are :</h5>
										<ol>
											<li>Next Day COD.</li>
											<li>Zero volumetric weight mismatch.</li>
											<li>Flagging of high risky COD orders.</li>
											<li>COD and NDR confirmation over what's app.</li>
										</ol>
									</td>
								</tr> <!-- COPY --> 
								<tr>
									<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
										<p style="margin: 0;">In case of any query contact us at 80057-47571 or email us at <a href="mailto:merchant@courierhome.com" title="merchant@courierhome.com">merchant@courierhome.com</a> </p>
									</td>
								</tr>
								<tr>
									<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
										<p style="margin: 0;">Cheers,<br><?php echo _e(CHMPLUGINNAME); ?> Team</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td  align="center" style="padding: 30px 10px 0px 10px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
								<tr>
									<td bgcolor="#FFF" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
										<h2 style="font-size: 20px; font-weight: 400; color: #111111; margin: 0;">Need more help?</h2>
										<p style="margin: 0;"><a href="<?php echo _e(CHMPLUGINURI); ?>contact/" target="_blank" style="color: #FFA73B;">We&rsquo;re here to help you out</a></p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td  align="center" style="padding: 0px 10px 0px 10px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
								<tr>
									<td  align="left" style="padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;"> <br>
										<!-- <p style="margin: 0;">If these emails get annoying, please feel free to <a href="#" target="_blank" style="color: #111111; font-weight: 700;">unsubscribe</a>.</p> -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div> 
			<style type="text/css">  
        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style> 
		<?php } ?>
	</div> 
</div> <!-- End chm-wrap -->

