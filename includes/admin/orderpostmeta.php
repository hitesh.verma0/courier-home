<?php global $post;  
 $shipmentOrderId	=get_post_meta($post->ID, 'chm_shipment_track_id', true); 
 if(!empty($shipmentOrderId)){
?>	
<div id="chmWooshipmentCurrentStatusBtn" class="postbox" style="text-align: center;padding: 10px 0px 10px 0px;" class="">
	 <a href="javascript:void(0)" id="chmsubmitbtn" onclick="chmgetShipmentDetails(<?php echo $shipmentOrderId;?>)" class="button save_order button-primary">
		 <span  style="margin-top: 5px;" class="dashicons dashicons-location"></span> Catch Order!</a>
<!-- 		 
		 <a href="javascript:void(0)" id="chmCancelShipmentbtn" onclick="chmCancelShipment(<?php echo $shipmentOrderId;?>)" class="button save_order button-secondary">
		 <span  style="margin-top: 5px;" class="dashicons dashicons-dismiss"></span> Cancel Shipment</a> -->
	 <!-- <p> <span style="    font-size: 40px;" class="dashicons dashicons-smiley"></span> </p> -->
 </div> 

<div id="chmWooshipmentCurrentStatus" class="postbox" style="display: none;">
	<ul class="ch_order_notes">
			<li rel="2" class="note ch_system">
			<div class="ch_note_content" >
				<p id="chmshipmentdes"> </p>
			</div>
			<p class="meta chmcurrentdate">
			<span class="dashicons dashicons-calendar"></span>
			<abbr class="exact-date" id="chmShipCurrentDate"></abbr> 
			</p>
			<p class="meta chmcurrentlocation"> 
				<span  class="dashicons dashicons-location"></span>
				<abbr class="exact-location" id="chmShipLocation"></abbr>  
				<a href="#chm_shipment_status" class="ch_more_orderdetail" role="button">Complete status</a>
			</p>
		</li>
	</ul> 
 </div> 
 <div class=""  style="text-align: center;">
 		<a href="javascript:void(0)" id="chmCancelShipmentbtn" onclick="chmCancelShipment(<?php echo $shipmentOrderId;?>)" class="button save_order button-secondary"> <span  style="margin-top: 5px;" class="dashicons dashicons-dismiss"></span> Cancel Shipment</a>
 </div>
 <?php }else{
	 echo "This Order is not synced with ".CHMPLUGINNAME;
 } ?>