<?php

/*Create tables in database during pluin activation*/
function chm_installer(){  
		global $wpdb;  
		$sql1 ="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix.CHMPRIFIX."warehouses` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`sender_name` varchar(255) NULL,
			`email` varchar(255) NULL,
			`mobile` varchar(15) NOT NULL,
			`address` varchar(255) NOT NULL, 
			`pincode` varchar(8) NULL,
			`status` TINYINT(2) NOT NULL DEFAULT '0',
			`created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
		 /*
		$sql2 = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix.CHMPRIFIX."order_tracks` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`tracking_id` int(11) NOT NULL,
			`shipment_status` int(11) NOT NULL,
			`modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
		  */
    require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
    dbDelta($sql1);
    // dbDelta($sql2); 
	// add option is plugin activate or not 
	chm_add_option();
	
}  
function chm_add_option(){
	if ( get_option('is_chm_actived')!== false ) {
		update_option('is_chm_actived','1');
		update_option('is_chm_authed','0');
	}else {
		add_option('is_chm_actived','1',null,'no');
		add_option('is_chm_authed','0',null,'no');
	} 
} 

function chm_deactivation(){ 
 	// add option is plugin activate or not 
	update_option('is_chm_actived','0');
	update_option('is_chm_authed','0');
	update_option('chm_authed_api_key','0');
	update_option('chm_authed_secret_key','0');
}


function chm_orderMetabox($user){ 
	// Removed the tracking functionality
	/* add_meta_box('chm_order_id', 'Courier Home Shipment', 'chm_orderPostmeta', array("shop_order"), 'side', 'high');
	add_meta_box('chm_shipment_status', 'Courier Home Shipment Status', 'chm_orderShipmentStatus', array("shop_order"), 'normal', 'default');*/
  }
  add_action('add_meta_boxes', 'chm_orderMetabox');