<?php 
/**
 * Plugin Name: Courier Home
 * Plugin URI: https://www.courierhome.com/
 * Description: Courier home is a one-stop destination for your courier needs from Hyperlocal deliveries to imports and Exports. Courierhome offers hyperlocal delivery, domestic and international courier.
 * Version: 1.4
 * Requires at least: 5.4
 * Requires PHP:      7.2
 * Author: Courier Home
 * Author URI: #
 * License: GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:  courierhome.com
 * Domain Path:  #
 */

   
/*define contstant */ 
$pluginName="Courier Home";
$pluginURI="https://www.courierhome.com/";
if( !function_exists('get_plugin_data') ){
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    $plugin_data= get_plugin_data(__FILE__);
    $pluginName=$plugin_data["Name"];
    $pluginURI=$plugin_data["PluginURI"]; 
}
define("CHMPLUGINNAME",$pluginName);
define("CHMPLUGINURI", $pluginURI);
define("CHM_DIR", dirname(__FILE__));
define("CHM_DIR_PATH", plugin_dir_path(__FILE__)); 
define('CHM_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CHMPRIFIX',"chm_"); 
define('APIBASEURL',"https://www.courierhome.com/apis/");  // Put courier home api

$urlparts = parse_url(home_url());
$domain = (!empty($urlparts))?$urlparts['host']:"";  
 define('CHM_DOMAIN',$domain);
require_once(CHM_DIR_PATH . "includes/chm-installer.php"); 
require_once(CHM_DIR_PATH . "includes/traits/webhooks.php");   // using for get wooCommerce products
require_once(CHM_DIR_PATH . "includes/traits/class-chm-dbquery.php");  
require_once(CHM_DIR_PATH . "includes/class/class-chm-base.php"); 
require_once(CHM_DIR_PATH . "includes/admin/chm-admin.php"); 
require_once(CHM_DIR_PATH . "includes/class/class-chm-api.php");  

$baseObj=new ChmBase();
$baseObj->chmInitWpFilter();

$adminObj=new chmAdmin();
$adminObj->chmInitAdminWpFilter();

$request=$_REQUEST;
// /* Only for Login */ 
if(isset($request['action'])&&!empty($request['action'])){ 
    $apiObj=new ChmApi();
    $apiObj->_chmCallApi(trim($request['action']));  
}


/* Plugin activation */
register_activation_hook( __FILE__, 'chm_installer' );
register_deactivation_hook(__FILE__, 'chm_deactivation');

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'chm_settings_link' );
function chm_settings_link( array $links ) {
    $url = admin_url() . "admin.php?page=chm-started";
    $settings_link = '<a href="' . $url . '">' . __('Settings', 'textdomain') . '</a>';
      $links[] = $settings_link;
    return $links;
}