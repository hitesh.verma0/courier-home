=== COURIER HOME ===
Contributors: CourierHome
Tags: Courier, COD, Shipment, Product Delivery,E-Commerce, Woocommerce, Case on delivery,Premium Content,Next Day COD
Requires at least: 5.6
Tested up to: 8.2
Requires PHP: 7.2
Stable tag: 1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Courier home is a one-stop destination for your courier needs from Hyperlocal deliveries to imports and Exports. Courierhome offers hyperlocal delivery, domestic and international courier.

### Features 

* <strong>COD</strong>: Next Day COD. 
* <strong>COD</strong>: Zero volumetric weight mismatch.  


###  Shortcodes 
 

### Support

We try our best to provide support on WordPress.org forums. However, We have a special [team support](https://www.courierhome.com/contact/) where you can ask us questions and get help. Delivering a good user experience means a lot to us and so we try our best to reply each and every question that gets asked.

### Credits 

== Frequently Asked Questions ==

= How to install and use this plugin? =

After you Active this plugin, just go to Dashboard > courier home > Settings, and then setup the default settings, after that, Add Order Pickup Address mean you warehouse address!

= How do I report bugs and suggest new features? =

You can report the bugs for this CourierHome plugin [here](https://www.courierhome.com/contact/)

= Will you include features to my request? =

Yes, Absolutely! We would suggest you submit your feature request [here](https://www.courierhome.com/contact/)

= How do I get in touch? =
You can contact us from [here](https://www.courierhome.com/contact/)

= Disclaimer =
 
A lot of studies has been done while making this plugin. Still the plugin is in beginning state. If you're new to WordPress or have a very limited technical background you may consider seeking out professional help your first time using the plugin. 
For any kind of assistance from our side, you can post your suggestions on our support section.


== Installation ==

* Upload `courier-home` plugin to the `/wp-content/plugins/` folder and Activate it.
* It will add a menu 'Courier Home' in the admin panel in the left menu. 

== Changelog ==

= 1.0 =
*Release Date - 03 January 2021*

* The very first release of plugin. Add a menu 'Courier Home' at the left menu of the admin panel.




