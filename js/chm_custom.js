jQuery(document).ready(function($) { 
  jQuery("#chm-removemsg").click(function(){
    jQuery("#chm-msgbox").remove();
  });

  jQuery('#chm-auth-form').submit(function(e) {
    e.preventDefault();  
    jQuery("#chm-submit").attr('disabled', 'disabled');
    jQuery("#loader").show();
    var api_key = jQuery("#chm_api_key").val();
    var secret_key = jQuery("#chm_secret_key").val(); 
    if (api_key != "") {
      chmShowLoadingIndicator();
      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: ({
          action: 'chm_check_auth',
          chm_api_key: api_key,
          chm_secret_key: secret_key
        }),
        success: function(response) {
          chmhideLoadingIndicator();
          jQuery("#chm-submit").removeAttr('disabled');
          jQuery("#loader").hide();
          var obj = JSON.parse(response);
          var html='<div class="chm-alert chm-alert-danger chm-alert-dismissible"> <span id="errormsg">' + obj.message + '</span></div>';
          if (obj.status != 'failed') {
            var html='<div class="chm-alert chm-alert-success chm-alert-dismissible"> <span id="errormsg">' + obj.message + '</span></div>'; 
            jQuery("#errormsgbx").html(html).show();
            window.location.href = window.location.href;
          }
          jQuery("#errormsgbx").html(html).show();
        }
      });
    }
  }); 


  jQuery('#chmwarehouseform').submit(function(e) {
    e.preventDefault();   
    jQuery("#loader").show(); 
    var chmAddress = jQuery("#chm-address").val(); 
    var chmEmail = jQuery("#chm-email").val();
    var chmDistrict = jQuery("#chm-district").val();  
    var chmPincode= jQuery("#chm-pincode").val();  
    var chmRedirect = jQuery("#chm-redirection").val();
    var chmMobile = jQuery("#chm-mobile").val();
    var chmName = jQuery("#chm-name").val();
    
    if (chmPincode != "" && chmAddress != "" && chmMobile != "") {
      chmShowLoadingIndicator();
      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: ({
          action: 'chm_check_auth',
          chmName: chmName,
          chmEmail: chmEmail, 
          chmMobile: chmMobile,
          chmAddress: chmAddress, 
          chmPincode: chmPincode,
        }),
        success: function(response) { 
          chmhideLoadingIndicator();
          jQuery("#loader").hide();
          var obj = JSON.parse(response);
          var html='<div class="chm-alert chm-alert-danger chm-alert-dismissible"> <span id="errormsg">' + obj.message + '</span></div>';
          if (obj.status != 'failed') {
            var html='<div class="chm-alert chm-alert-success chm-alert-dismissible"> <span id="errormsg">' + obj.message + '</span></div>'; 
          } 
          jQuery("#errormsgbx").html(html).show();
          if (obj.status != 'failed') {
            if(chmRedirect!=""){
              window.location.href = chmRedirect;  
            }else{
              window.location.href = window.location.href;
            }
          }
          jQuery("#chmsubmit").removeAttr('disabled');
        }
      });
    }
  }); 
});


function chmgetShipmentDetails(trackId){
  chmShowLoadingIndicator();
  jQuery("#chmsubmitbtn").attr('disabled', 'disabled');
  jQuery.ajax({
    url: ajaxurl,
    type: 'POST',
    data: ({
      action: 'chm_trackshipment',
      track: trackId, 
    }),
    success: function(response) {
      chmhideLoadingIndicator();
      jQuery("#chmsubmitbtn").removeAttr('disabled');
       
      var obj = JSON.parse(response);
       
      //  console.log(obj);
      if (obj.status != 'failed') {
       
        var currentShipment=obj.data.currentShipment;
        // console.log(currentShipment);
        chm_mapShipmentStatus(obj.data.tracking);
        jQuery("#chmWooshipmentCurrentStatusBtn").hide();
        
        jQuery("#chmshipmentdes").replaceWith('<p class="chmshipmentdes">'+currentShipment.UpdateDescription+'</p>');
        jQuery("#chmShipCurrentDate").replaceWith('<abbr class="exact-date" title="'+currentShipment.UpdateDateTime+'" id="chmShipCurrentDate">'+currentShipment.UpdateDateTime+'</abbr>');
        jQuery("#chmShipLocation").replaceWith('<abbr class="exact-location" title="'+currentShipment.UpdateLocation+'" id="chmShipLocation">'+currentShipment.UpdateLocation+'</abbr>');
         
        jQuery("#chmWooshipmentCurrentStatus").show();
      }else{
        alert(obj.message); 
      }   
    }
  });
}
function chm_mapShipmentStatus(chmshipments){
  
  chmshipments.forEach(function(chmshipment,count) {   
    var html='<tr class="iedit author-self level-0 post-67 type-shop_order status-wc-processing post-password-required hentry"><td>'+(eval(count)+eval(1))+'</td><td><p id="chmshipmentlistdes">'+chmshipment.UpdateDescription+'</p></td><td>  <abbr class="exact-location" id="chmShipListLocation">'+chmshipment.UpdateLocation+'</abbr></td><td class="meta"><abbr class="exact-date" id="chmShipListCurrentDate">'+chmshipment.UpdateDateTime+'</abbr></td></tr>';
    jQuery("#chm-shipmentul").append(html);
    }); 
}

function chmCancelShipment(trackId){
  if (confirm('Do You want to cancel this shipment!')) {
    jQuery("#chmCancelShipmentbtn").attr('disabled', 'disabled');
    chmShowLoadingIndicator();
    jQuery.ajax({
      url: ajaxurl,
      type: 'POST',
      data: ({
        action: 'chm_cacnelshipment',
        track: trackId, 
      }),
      success: function(response) {
        chmhideLoadingIndicator();
        jQuery("#chmCancelShipmentbtn").removeAttr('disabled'); 
        var obj = JSON.parse(response); 
        if (obj.status == 'success') {
          jQuery("#chmWooshipmentCurrentStatus").show(); 
          jQuery("#chmshipmentdes").replaceWith('<p class="chmshipmentdes">'+obj.message+'</p>');
          jQuery(".chmcurrentdate").hide();
          jQuery(".chmcurrentlocation").hide();
          //  window.location.href = window.location.href;  
        }else{
          alert(obj.message); 
        }   
      }
    });
  }
}

function chmShowLoadingIndicator() {
  html = '<div class="spinner-container"><div class="loader-main"><div class="cssload-loader"><div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div></div><p>Please wait ...</p></div></div>'; 
  jQuery('body').append(html);
}

function chmhideLoadingIndicator() {
  jQuery('.spinner-container').remove();
}


 


function isChmNumberField(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}